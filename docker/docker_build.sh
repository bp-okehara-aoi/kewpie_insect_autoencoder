cd ..

IMAGE=kewpie_insect_detection
VERSION=latest

docker build -t $IMAGE:$VERSION -f docker/Dockerfile .
