#!/bin/bash
IMAGENAME=kewpie_insect_detection
VERSION=latest
IMAGE=$IMAGENAME:$VERSION

docker container run \
  -v /Users/aoi.okehara/Devel/kewpie-insect-autoencoder:/app \
  --rm \
  -it \
  $IMAGE \
  bash

