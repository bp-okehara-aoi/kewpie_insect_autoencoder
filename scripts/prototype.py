import math

import joblib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow import keras


def load_data(path):
    with open(path) as f:
        df = pd.read_csv(f)
    df.iloc[:, 4:] = df.iloc[:, 4:].applymap(lambda x: complex(x))
    return df


def create_feature_amplitude_diff_ratio(df):
    df_amp = df.iloc[:, 4:].applymap(lambda x: np.log(x.real**2 + x.imag**2)
                                     if isinstance(x, complex) else x).copy()
    df_amp = df_amp.rename(columns=dict(
        zip([c for c in df_amp.columns],
            [c + '_amp' for c in df_amp.columns])),
        inplace=False)
    return df_amp


def create_feature_phaseshift_diff_ratio(df):
    df_ph_shift = df.iloc[:, 4:].applymap(lambda x: math.degrees(
        math.atan2(x.imag, x.real)) if isinstance(x, complex) else x).copy()
    df_ph_shift = df_ph_shift.rename(columns=dict(
        zip([c for c in df_ph_shift.columns],
            [c + '_arg' for c in df_ph_shift.columns])),
        inplace=False)

    ret_phaseshifts = []
    for _, phase_shift in df_ph_shift.iterrows():
        if phase_shift[0] > 0:
            bias = 0
        else:
            bias = 360
        phaseshifts_around = []
        for i, ps in enumerate(phase_shift):
            phaseshifts_around.append(phase_shift[i] + bias)
            if i + 1 < len(phase_shift) and phase_shift[
                    i + 1] > 0 and phase_shift[i] < 0:
                bias -= 360
        ret_phaseshifts.append(phaseshifts_around)

    return pd.DataFrame(ret_phaseshifts, columns=df_ph_shift.columns)


def get_reconst_diff(model, scaler, df):
    scaled = scaler.transform(df)
    preds = model.predict(scaled)
    diff_df = pd.DataFrame(scaled - preds, columns=df.columns)
    return diff_df


if __name__ == "__main__":
    # 入力データの生成
    df = load_data('../data/data_0423_0cm.csv')
    df_amp = create_feature_amplitude_diff_ratio(df)
    df_ph_shift = create_feature_phaseshift_diff_ratio(df)
    df_input = pd.concat([df.iloc[:, :4], df_amp, df_ph_shift], axis=1)

    # データの分割
    X_train, X_test = train_test_split(df_input, test_size=0.2, random_state=0)
    X_train, X_valid = train_test_split(X_train, test_size=0.2, random_state=0)
    print('dat size', '-' * 100)
    print('train : ', X_train.shape)
    print('valid : ', X_valid.shape)
    print('test : ', X_test.shape)

    # 入力データの正規化
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train.iloc[:, 4:])
    X_valid = scaler.transform(X_valid.iloc[:, 4:])
    joblib.dump(scaler, '../models/scaler.pkl')

    # AE良品モデルの作成
    dat_shape = X_train.shape[1]
    input_ = keras.layers.Input(shape=(dat_shape, ))
    x = keras.layers.Dense(30,
                           activation="selu",
                           kernel_initializer="lecun_normal")(input_)
    x = keras.layers.Dense(30,
                           activation="selu",
                           kernel_initializer="lecun_normal")(x)
    x = keras.layers.Dense(30,
                           activation="selu",
                           kernel_initializer="lecun_normal")(x)
    output_ = keras.layers.Dense(dat_shape)(x)
    model = keras.Model(inputs=input_, outputs=output_)
    optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999)
    model.compile(loss="mse", optimizer=optimizer)
    early_stopping_cb = keras.callbacks.EarlyStopping(
        patience=10, restore_best_weights=True)

    # モデルの学習
    history = model.fit(X_train,
                        X_train,
                        epochs=10000,
                        validation_data=(X_valid, X_valid),
                        callbacks=[early_stopping_cb])
    model.save('../models/AE_model.h5')

    # テスト良品データでの復元誤差取得
    diff_df_norm = get_reconst_diff(model, scaler, X_test.iloc[:, 4:])

    # AE不良品データの整形
    df_anorm = load_data('../data/data_0423_5cm.csv')
    df_amp_anorm = create_feature_amplitude_diff_ratio(df_anorm)
    df_ph_shift_anorm = create_feature_phaseshift_diff_ratio(df_anorm)
    df_anorm_input = pd.concat([df_amp_anorm, df_ph_shift_anorm], axis=1)

    # 不良品データの復元誤差取得
    diff_df_anorm = get_reconst_diff(model, scaler, df_anorm_input)

    # 可視化
    diff_df_anorm['label'] = 1
    diff_df_norm['label'] = 0
    # PCA
    pca_dat = pd.concat([diff_df_anorm, diff_df_norm], axis=0)
    pca = PCA(random_state=0)
    decomposed = pca.fit_transform(pca_dat.iloc[:, :-1])
    # 第一PCx第二PC
    split_idx = diff_df_anorm.shape[0] + 1
    fig, ax = plt.subplots()
    ax.scatter(decomposed[:split_idx, 0],
               decomposed[:split_idx, 1],
               alpha=0.8,
               c='tab:blue',
               label='anorm')
    ax.scatter(decomposed[split_idx:, 0],
               decomposed[split_idx:, 1],
               alpha=0.8,
               c='tab:red',
               label='norm')
    ax.legend()
    ax.set_xlabel("PC_0")
    ax.set_ylabel("PC_1")
    fig.savefig("../results/pca_plot.png")
    # top10PC
    sns_df = pd.DataFrame(decomposed).iloc[:, :10]
    sns_df.columns = [f'PC_{col}' for col in sns_df.columns]
    sns_df.loc[sns_df.index < split_idx, 'label'] = 'anorm'
    sns_df.loc[sns_df.index >= split_idx, 'label'] = 'norm'
    sns.pairplot(sns_df, hue='label', plot_kws={
        'alpha': 0.4
    }).savefig('../results/pair_plot.png')
